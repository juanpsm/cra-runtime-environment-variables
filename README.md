# Tutorial [How to implement runtime environment variables with create-react-app, Docker, and Nginx](https://www.freecodecamp.org/news/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70/)


## Prereqs

- npm
- docker
- docker-compose
- yarn

```bash
npm install --global yarn
yarn install
```

This app was created using

```bash
npx create-react-app cra-runtime-environment-variables  
```

## Run app 

### Dev

```bash
API_URL=https://my.new.dev.api.com yarn dev
```

### Prod

```bash
docker run -p 3000:80 -e API_URL=https://staging.api.com -t juanpsm/cra-runtime-environment-variables
```

```bash
docker-compose up
```